# Revenue Operations

## Purpose

The team aims to increase sales teams efficiency through processes all while generating high quality data for insights.

## Main OKRs level 1 related

* CP-KR 1.3: Maintain “efficiency score” above 1.25x
* CP-KR 2.1: Achieve $10M ARR Target
* CP-KR 2.4b: Increase average win rate by 15%

## Members and functions

* [Paola Palhais](https://open.rocket.chat/direct/paola.palhais): Senior Sales Ops
  * Work on designing processes, project management and performance evaluation
