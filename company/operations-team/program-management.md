# Program Management

## Purpose

To extend the mindset of generating new business to all departments of the organization. Responsible for tech stack within the revenue generating teams, focused on increasing efficiency through new tools and integrations between existing ones. We develop and maintain key processes, systems, tools, and reporting for corporate management.

## Main OKRs level 1 related

* CP-KR 1.2: Average 94% “favorability” rating
* CP-KR 2.1: Achieve $10M ARR Target
* CP-KR 2.3b: Drive 3,000 SQL’s
* CP-KR 2.4b: Increase average win rate by 15%
* CP-KR 3.5: Reduce Gross MRR Churn Rate by 50%

## Members and functions

* [Marcelo Schmidt](https://open.rocket.chat/direct/marcelo.schmidt): Program Manager. 
  * Responsible for building a team of tech operations and currently managing the tasks at hand.

{% tabs %}
{% tab title="Key Initiatives" %}
| **Domain** |  |
| :--- | :--- |


| % of time | 30% |
| :--- | :--- |


| Objective | To extend the mindset of generating new business to all departments of the organization. |
| :--- | :--- |


| Primary function | Create and optimize processes, define the hiring guidelines and provide tools, training and inputs to improve routines and performance of salespeople. |
| :--- | :--- |


| Secondary function | Periodically review the strategy and results of our current sales enablement process. |
| :--- | :--- |


<table>
  <thead>
    <tr>
      <th style="text-align:left">Areas of focus</th>
      <th style="text-align:left">
        <ul>
          <li>Sales Enablement (Open LMS)</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>

<table>
  <thead>
    <tr>
      <th style="text-align:left">Key results</th>
      <th style="text-align:left">
        <ul>
          <li>Achieve participation in training</li>
          <li>Increase sales productivity</li>
          <li>Decrease sales cycle</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>
{% endtab %}

{% tab title="System & Tools" %}
| **Domain** |  |
| :--- | :--- |


| % of time | 30% |
| :--- | :--- |


| Objective | Responsible for tech stack within the revenue generating teams, focused on increasing efficiency through new tools and integrations between existing ones. |
| :--- | :--- |


| Primary function | Automate repetitive or tedious manual tasks. |
| :--- | :--- |


| Secondary function | Support our business intelligence team. |
| :--- | :--- |


<table>
  <thead>
    <tr>
      <th style="text-align:left">Areas of focus</th>
      <th style="text-align:left">
        <ul>
          <li>CRM</li>
          <li>Connectivity / Integration w FC, Hubspot, etc.</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>

<table>
  <thead>
    <tr>
      <th style="text-align:left">Key results</th>
      <th style="text-align:left">
        <ul>
          <li>Decrease dependency to other teams in sales cycle</li>
          <li>Decrease number of duplicates in CRM (or increase reliability)</li>
          <li>Improve customer NPS/CES scores (it&#x2019;s a side effect of automating,
            reducing time to answer)</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>
{% endtab %}

{% tab title="Corporate Management" %}
| **Domain** |  |
| :--- | :--- |


| % of time | 20% |
| :--- | :--- |


| Objective | Develop and maintain key processes, systems, tools, and reporting |
| :--- | :--- |


| Primary function | Create, manage and improve connection between areas, mapping the end-to-end process workflow. |
| :--- | :--- |


| Secondary function | Being an advisor and facilitator for the team to provide feedback and complaints. |
| :--- | :--- |


<table>
  <thead>
    <tr>
      <th style="text-align:left">Areas of focus</th>
      <th style="text-align:left">
        <ul>
          <li>Transitioning / support for Finance</li>
          <li>Annual strategic and budget planning</li>
          <li>Support for PEO / global corporate management function</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>

<table>
  <thead>
    <tr>
      <th style="text-align:left">Key results</th>
      <th style="text-align:left">
        <ul>
          <li>Achieve 100% of contracts recorded in Zoho Subscriptions</li>
          <li>Increase number of feedbacks received on Officevibe</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>
{% endtab %}

{% tab title="Culture" %}
| **Domain** |  |
| :--- | :--- |


| % of time | 20% |
| :--- | :--- |


| Objective | Develop and maintain key processes, systems, tools, and reporting |
| :--- | :--- |


| Primary function | Develop and put in practice culture programs such as Culture Month, Listening Day, Customer Day, Blend In and others.Secondary function |
| :--- | :--- |


| Secondary function | Develop bots for interacting with employees and gathering feedback and participation in culture initiatives. |
| :--- | :--- |


<table>
  <thead>
    <tr>
      <th style="text-align:left">Areas of focus</th>
      <th style="text-align:left">
        <ul>
          <li>Company Culture</li>
          <li>Ops Team Performance / Cohesion</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>

<table>
  <thead>
    <tr>
      <th style="text-align:left">Key results</th>
      <th style="text-align:left">
        <ul>
          <li>Increase the number of developed culture initiatives from 1 to 5</li>
          <li>Increase eNPS</li>
          <li>Increase customer NPS</li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody></tbody>
</table>
{% endtab %}
{% endtabs %}

## Initiatives, Leads and Milestones

{% tabs %}
{% tab title="OpenLMS setup" %}
### Function

Sales

### Lead

Marcelo

### Business Impact

Capacitate our sales personnel and partners on the knowledge necessary for identifying Rocket.Chat users who have a pressing need for enhanced capabilities, before making contact.

### **OKR Connection**

Level 1:

* CP-KR 2.1: Achieve $10M ARR Target
* CP-KR 2.4b: Increase average win rate by 15%

Level 2:

* OP-KR 3.2: Increase sales team productivity by 30% 
* OP-KR 3.3: Reach 6% conversion rate from trial to paid
* OP-KR 3.4: Decrease sales cycle \(duration\) by 20%

### **Biggest Deliverable \(or\) Value Milestone**

* March 19: OpenLMS setup ready for launch.
* TBD: First round of evaluation on the effectiveness of the course
{% endtab %}

{% tab title="Zoho.Bridge / Fleetcommand Sync" %}
### Function

Operations

### Lead

Marcelo

### Business Impact

Keep our data synced between our multiple platforms: FleetCommand, CRM, Desk, Hubspot

### **OKR Connection**

Level 1:

* CP-KR 2.3b: Drive 3,000 SQL’s
* CP-KR 3.5: Reduce Gross MRR Churn Rate by 50%

Level 2:

* OP-KR 3.2: Increase sales team productivity by 30%
* OP-KR 4.3: Properly address support SLA

### **Biggest Deliverable \(or\) Value Milestone**

* Matching records on different systems must not have different data
* Data can flow from any system to any other system
{% endtab %}

{% tab title="Culture Guardian" %}
### Function

Company Culture

### Lead

Marcelo

### Business Impact

Create a trust mechanism for employees to get support to their demands and complaints.

### **OKR Connection**

Level 1:

* CP-KR 1.2: Average 94% “favorability” rating

Level 2:

* OP-KR 2.5: Establish and maintain a strong Culture “Guardian”

### **Biggest Deliverable \(or\) Value Milestone**

* Employees can safely report their concerns
{% endtab %}

{% tab title="IT System Plan" %}
### Function

Operations

### Lead

Marcelo

### Business Impact

To handle the planning and execution of key IT systems for Rocket.Chat, including Zoho suite, Fleetcommand, Hubdoc and others

### **Biggest Deliverable \(or\) Value Milestone**

* Systems communicate and are reliable
{% endtab %}
{% endtabs %}

